package com.hirecomment.service;

import com.hirecomment.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 管理员表  服务类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
public interface AdminService extends IService<Admin> {

}
