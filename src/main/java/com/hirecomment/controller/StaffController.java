package com.hirecomment.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 职员表  前端控制器
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
@RestController
@RequestMapping("/staff")
public class StaffController {

}

