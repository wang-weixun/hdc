package com.hirecomment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HdcApplication {

    public static void main(String[] args) {
        SpringApplication.run(HdcApplication.class, args);
    }

}
