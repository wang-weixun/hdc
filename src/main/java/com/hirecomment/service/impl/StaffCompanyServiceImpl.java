package com.hirecomment.service.impl;

import com.hirecomment.entity.StaffCompany;
import com.hirecomment.mapper.StaffCompanyMapper;
import com.hirecomment.service.StaffCompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
@Service
public class StaffCompanyServiceImpl extends ServiceImpl<StaffCompanyMapper, StaffCompany> implements StaffCompanyService {

}
