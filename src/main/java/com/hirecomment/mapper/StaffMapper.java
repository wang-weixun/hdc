package com.hirecomment.mapper;

import com.hirecomment.entity.Staff;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 职员表  Mapper 接口
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
public interface StaffMapper extends BaseMapper<Staff> {

}
