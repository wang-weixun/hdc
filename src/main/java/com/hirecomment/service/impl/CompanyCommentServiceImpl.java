package com.hirecomment.service.impl;

import com.hirecomment.entity.CompanyComment;
import com.hirecomment.mapper.CompanyCommentMapper;
import com.hirecomment.service.CompanyCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
@Service
public class CompanyCommentServiceImpl extends ServiceImpl<CompanyCommentMapper, CompanyComment> implements CompanyCommentService {

}
