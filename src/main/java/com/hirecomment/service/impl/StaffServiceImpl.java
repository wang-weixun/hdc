package com.hirecomment.service.impl;

import com.hirecomment.entity.Staff;
import com.hirecomment.mapper.StaffMapper;
import com.hirecomment.service.StaffService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 职员表  服务实现类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
@Service
public class StaffServiceImpl extends ServiceImpl<StaffMapper, Staff> implements StaffService {

}
