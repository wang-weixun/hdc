package com.hirecomment.mapper;

import com.hirecomment.entity.Admin;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 管理员表  Mapper 接口
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
public interface AdminMapper extends BaseMapper<Admin> {

}
