package com.hirecomment.service;

import com.hirecomment.entity.Staff;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 职员表  服务类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
public interface StaffService extends IService<Staff> {

}
