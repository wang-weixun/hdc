package com.hirecomment.service.impl;

import com.hirecomment.entity.Company;
import com.hirecomment.mapper.CompanyMapper;
import com.hirecomment.service.CompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company> implements CompanyService {

}
