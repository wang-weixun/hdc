package com.hirecomment.mapper;

import com.hirecomment.entity.Company;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
public interface CompanyMapper extends BaseMapper<Company> {

}
