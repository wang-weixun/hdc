package com.hirecomment.service;

import com.hirecomment.entity.Company;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
public interface CompanyService extends IService<Company> {

}
