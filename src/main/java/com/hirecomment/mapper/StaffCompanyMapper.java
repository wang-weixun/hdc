package com.hirecomment.mapper;

import com.hirecomment.entity.StaffCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
public interface StaffCompanyMapper extends BaseMapper<StaffCompany> {

}
