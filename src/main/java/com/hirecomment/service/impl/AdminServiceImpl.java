package com.hirecomment.service.impl;

import com.hirecomment.entity.Admin;
import com.hirecomment.mapper.AdminMapper;
import com.hirecomment.service.AdminService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 管理员表  服务实现类
 * </p>
 *
 * @author Nigori-wwx
 * @since 2021-05-02
 */
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin> implements AdminService {

}
